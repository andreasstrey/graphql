package ch.strey.graphqltest.grapqldto;

import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Expert {

	private String expertNo;
	private String expertId;
	private String language;
	private String country;
	private String firstName;
	private String lastName;
	private String photoPath;
	private String actualAdvice;
	private List<Terminal> terminals;
	private String status;
	private List<Map<String, String>> expertSwitches;
	private List<Map<String, String>> rota;
}
