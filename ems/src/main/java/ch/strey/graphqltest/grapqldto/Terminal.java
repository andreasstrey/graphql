package ch.strey.graphqltest.grapqldto;

import lombok.Data;

@Data
public class Terminal {
	private String terminalNo;
	private String chargeType;
	private String number;
	private boolean active;
}
