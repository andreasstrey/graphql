package ch.strey.graphqltest.grapqldto;

import java.util.Map;

import lombok.Data;

@Data
public class ExpertLogin {
	private Map<String, String> token;
	private Expert expert;
}
