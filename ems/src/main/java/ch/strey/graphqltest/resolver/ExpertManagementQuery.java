package ch.strey.graphqltest.resolver;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.strey.graphqltest.connector.ExpertManagementConnector;
import ch.strey.graphqltest.grapqldto.Expert;
import ch.strey.graphqltest.grapqldto.ExpertLogin;
import ch.strey.graphqltest.grapqldto.Terminal;
import graphql.schema.DataFetchingEnvironment;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ExpertManagementQuery implements GraphQLQueryResolver {

	@Autowired
	private ExpertManagementConnector connector;

	@Autowired
	private ObjectMapper mapper;

	public ExpertLogin getExpertLogin(String mandator, String userName, String password) {
		Map<String, String> request = Stream
				.of(new AbstractMap.SimpleEntry<>("userName", userName),
						new AbstractMap.SimpleEntry<>("password", password))
				.collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()));

		return connector.getExpertLogin(request, mandator);
	}

	public Expert getExpert(String token, String mandator, String expertNo, int productTypeParentGroup,
			DataFetchingEnvironment env) {
		JsonNode expert = connector.getExpert(token, mandator, expertNo);
		Expert.ExpertBuilder builder = Expert.builder().expertNo(expert.at("/expert/expertNo").asText())
				.expertId(expert.at("/expert/expertId").asText()).language(expert.at("/expert/language").asText())
				.country(expert.at("/expert/country").asText()).firstName(expert.at("/expert/firstName").asText())
				.lastName(expert.at("/expert/lastName").asText()).photoPath(expert.at("/expert/photoPath").asText())
				.actualAdvice(expert.at("/expert/actualAdvice").asText())
				.terminals(mapper.convertValue(expert.at("/expert/terminals"), new TypeReference<List<Terminal>>() {
				})).status(expert.at("/expert/status").asText()).expertSwitches(mapper.convertValue(
						expert.at("/expert/expertSwitches"), new TypeReference<List<Map<String, String>>>() {
						}));

		if (env.getSelectionSet().contains("rota/*")) {
			log.info("requesting rota for expert: {}", expertNo);
			JsonNode rota = connector.getRota(token, mandator, expertNo, productTypeParentGroup);
			builder.rota(mapper.convertValue(rota.at("/rota"), new TypeReference<List<Map<String, String>>>() {
			}));
		}
		return builder.build();
	}
}
