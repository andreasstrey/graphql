package ch.strey.graphqltest.connector;

import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.JsonNode;

import ch.strey.graphqltest.grapqldto.ExpertLogin;
import feign.Headers;

@org.springframework.cloud.openfeign.FeignClient(name = "expertmanagement", url = "${expertmanagement.url}")
public interface ExpertManagementConnector {

	@PostMapping("/{mandator}/login")
	@Headers("Content-Type: application/json")
	public ExpertLogin getExpertLogin(Map<String, String> loginRequest, @PathVariable("mandator") String mandator);

	@GetMapping("/{mandator}/experts/{expertNo}")
	public JsonNode getExpert(@RequestHeader("token") String token, @PathVariable("mandator") String mandator,
			@PathVariable("expertNo") String expertNo);

	@GetMapping("/{mandator}/experts/{expertNo}/rota")
	public JsonNode getRota(@RequestHeader("token") String token, @PathVariable("mandator") String mandator,
			@PathVariable("expertNo") String expertNo,
			@RequestParam("productTypeParentGroup") int productTypeParentGroup);
}
