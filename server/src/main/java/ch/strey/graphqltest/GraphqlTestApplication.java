package ch.strey.graphqltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class GraphqlTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlTestApplication.class, args);
	}
}
