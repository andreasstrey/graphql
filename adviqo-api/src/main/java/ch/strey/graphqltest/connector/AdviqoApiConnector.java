package ch.strey.graphqltest.connector;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.strey.graphqltest.responses.BaseResponse;

@Service
public class AdviqoApiConnector {

	private String output = "JSON";

	@Value("${adviqoApi.appversion}")
	private String appversion;

	@Value("${adviqoApi.authid}")
	private String authid;

	@Value("${adviqoApi.appid}")
	private String appid;

	@Value("${adviqoApi.authkey}")
	private String authkey;

	@Value("${adviqoApi.adid}")
	private String adid;

	@Value("${adviqoApi.uuid}")
	private String uuid;

//	@Value("${adviqoApi.sessionId}")
//	private String sessionId; //TODO: get from request

	private RestTemplate restTemplate;

	public BaseResponse getCategoryResponse(String request, String sessionId, final int categoryNo) {

		// req=category
		restTemplate = new RestTemplate();

		String ems_url = "https://www.questico.de/k3/adviqoAPI?";

		ems_url = ems_url.concat("req=" + request).concat("&").concat("category=" + categoryNo).concat("&")
				.concat("appversion=" + appversion).concat("&").concat("authid=" + authid).concat("&")
				.concat("appid=" + appid).concat("&").concat("authkey=" + authkey).concat("&").concat("adid=" + adid)
				.concat("&").concat("uuid=" + uuid).concat("&").concat("sessionId=" + sessionId).concat("&")
				.concat("output=" + output);

		System.out.println(ems_url);

		ResponseEntity<BaseResponse> response = restTemplate.getForEntity(ems_url, BaseResponse.class);

		return response.getBody();
	}
}
