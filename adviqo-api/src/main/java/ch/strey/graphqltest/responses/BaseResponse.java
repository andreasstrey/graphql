package ch.strey.graphqltest.responses;

import java.util.List;

import ch.strey.graphqltest.dto.Category;
import ch.strey.graphqltest.dto.Member;

public class BaseResponse {

	private Member memberData;
	private List<Category> categoryResponse;

	public BaseResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BaseResponse(Member memberData, List<Category> categoryResponse) {
		super();
		this.memberData = memberData;
		this.categoryResponse = categoryResponse;
	}

	public Member getMemberData() {
		return memberData;
	}

	public void setMemberData(Member memberData) {
		this.memberData = memberData;
	}

	public List<Category> getCategoryResponse() {
		return categoryResponse;
	}

	public void setCategoryResponse(List<Category> categoryResponse) {
		this.categoryResponse = categoryResponse;
	}

}
