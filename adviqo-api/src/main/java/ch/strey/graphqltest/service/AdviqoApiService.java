package ch.strey.graphqltest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.strey.graphqltest.connector.AdviqoApiConnector;
import ch.strey.graphqltest.responses.BaseResponse;

@Service
public class AdviqoApiService {

	@Autowired
	private AdviqoApiConnector adviqoApiConnector;
	
	public BaseResponse getAdviqoApiResponse(String request, String sessionId, final int categoryNo) {
		return adviqoApiConnector.getCategoryResponse(request, sessionId, categoryNo);
	}
}
