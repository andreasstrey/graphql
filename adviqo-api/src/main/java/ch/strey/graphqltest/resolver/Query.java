package ch.strey.graphqltest.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import ch.strey.graphqltest.dto.Category.Listings;
import ch.strey.graphqltest.grapqldto.CategoryResponse;
import ch.strey.graphqltest.service.AdviqoApiService;

@Component
public class Query implements GraphQLQueryResolver {

	@Autowired
	private AdviqoApiService adviqoApiService;

	public CategoryResponse getCategory(String sessionId, int categoryNo) {
		Listings result = adviqoApiService.getAdviqoApiResponse("category", sessionId, categoryNo).getCategoryResponse().get(0)
				.getListings();
		System.out.println(result);

		return new CategoryResponse(result);
	}

}
