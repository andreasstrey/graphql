package ch.strey.graphqltest.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {

	private Listings listings;


	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public class Listings {

		private String categoryId;
		private String slogan;
		private Long category;
		private boolean showGratis;
		private int totalCount;
		private int startNo;
		private int pageSize;
		private List<Listing> listing;
	}
}
