package ch.strey.graphqltest.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Member {

	@JsonProperty(value = "memberId")
	private String memberId;

	@JsonProperty(value = "memberTypeNo")
	private Integer memberTypeNo;

	@JsonProperty(value = "loggedin")
	private boolean loggedin;

	@JsonProperty(value = "gratisCall")
	private boolean gratisCall;

	@JsonProperty(value = "lastName")
	private String lastName;

	@JsonProperty(value = "firstName")
	private String firstName;

	@JsonProperty(value = "phoneNumber")
	private String phoneNumber;

	@JsonProperty(value = "nationalPhoneNumber")
	private String nationalPhoneNumber;

	@JsonProperty(value = "email")
	private String email;

	@JsonProperty(value = "country")
	private String country;

	@JsonProperty(value = "currency")
	private String currency;

	@JsonProperty(value = "terminal")
	private List<Object> terminal;

	@JsonProperty(value = "no")
	private String memberNo;

	@Override
	public String toString() {
		return "Member [memberId=" + memberId + ", memberTypeNo=" + memberTypeNo + ", loggedin=" + loggedin
				+ ", gratisCall=" + gratisCall + ", lastName=" + lastName + ", firstName=" + firstName
				+ ", phoneNumber=" + phoneNumber + ", nationalPhoneNumber=" + nationalPhoneNumber + ", email=" + email
				+ ", country=" + country + ", currency=" + currency + ", terminal=" + terminal + ", memberNo="
				+ memberNo + "]";
	}
	
}
