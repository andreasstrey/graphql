package ch.strey.graphqltest.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
	private String productType;
	private double priceStationary;
	private double priceMobile;
	private int productTypeParentGroup;
	private String availability;
	private List<Object> timedSlot; // TODO: check the type
	private List<Object> packages; // TODO: check the type
	private List<Object> packageList; // TODO: check the type

}
