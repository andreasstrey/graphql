package ch.strey.graphqltest.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Listing {

	private Long listingNo;
	private String listingId;
	private Long expertNo;
	private String expertId;
	private List<Product> product;
	private int count;
	private String stars;
	private String expertPhotoPath;
	private String expertCode;
	private String expertKeyword;
	private String shortDescription;
	private List<Integer> partnerNo;
	private double ratingAverage;
	private double ratingPercent;
	private String mainCategoryNo;
	private List<String> logoPath;
	private String languages;

}