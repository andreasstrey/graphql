package ch.strey.graphqltest.grapqldto;

import java.util.List;
import java.util.stream.Collectors;

import ch.strey.graphqltest.dto.Category.Listings;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponse {
	private int totalCount;

	private int offset; // startNo

	private int limit; // pageSize

	private Category category;

	private List<Listing> listings;

	public CategoryResponse(Listings listings) {
		this.totalCount = listings.getTotalCount();
		this.offset = listings.getStartNo();
		this.limit = listings.getPageSize();
		this.category = new Category(listings);
		this.listings = this.getListings(listings.getListing());
	}

	private List<Listing> getListings(List<ch.strey.graphqltest.dto.Listing> listings) {

		return listings.stream().map(l -> new Listing(l)).collect(Collectors.toList());
	}

}
