package ch.strey.graphqltest.grapqldto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Listing {
	private Long listingNo;
	private String listingId;
	private Long expertNo;
	private String expertId;
	private List<Product> products;
	private int count;
	private int stars;
	private String expertPhotoPath;
	private String expertCode;
	private String expertKeyword;
	private String shortDescription;
	private List<Integer> partnerNos;
	private double ratingAverage;
	private double ratingPecentage;
	private int mainCategoryNo;
	private List<String> logoPath;
	private List<String> languages;

	public Listing(ch.strey.graphqltest.dto.Listing listing) {
		this.listingNo = listing.getListingNo();
		this.listingId = listing.getListingId();
		this.expertNo = listing.getExpertNo();
		this.expertId = listing.getExpertId();
		this.count = listing.getCount();
		this.stars = Integer.valueOf(listing.getStars());
		this.expertPhotoPath = listing.getExpertPhotoPath();
		this.expertCode = listing.getExpertCode();
		this.expertKeyword = listing.getExpertKeyword();
		this.shortDescription = listing.getShortDescription();
		this.partnerNos = listing.getPartnerNo();
		this.ratingAverage = listing.getRatingAverage();
		this.ratingPecentage = listing.getRatingPercent();
		this.mainCategoryNo = Integer.valueOf(listing.getMainCategoryNo());
		this.logoPath = listing.getLogoPath();
		this.languages = this.getLanguages(listing.getLanguages());
	}

	private List<String> getLanguages(String languages) {
		List<String> result = new ArrayList<>();
		String[] splitted = languages.split("\\|");

		for (int i = 0; i < splitted.length; i++) {
			String current = splitted[i];
			if (!"".equals(current)) {
				result.add(current);
			}
		}
		return result;
	}
}
