package ch.strey.graphqltest.grapqldto;


import ch.strey.graphqltest.dto.Category.Listings;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {

	private long id; // category

	private String name; // categoryId

	private String slogan;

	private boolean showGratis;


//	private List<> listings;


	public Category(Listings listings) {
		
		this.id = listings.getCategory();
		this.name = listings.getCategoryId();
		this.slogan = listings.getSlogan();
		this.showGratis = listings.isShowGratis();
	}
	
	
}
